

# 					Projet Jeu JavaScript



## Introduction

Durée du projet : 3 semaines

L'objectif de ce projet est de faire un petit jeu en Javascript sans framework. Il va permettre d'utiliser le JS, l'algorithmie de base, la manipulation du DOM et la POO dans un contexte ludique... mais professionnel.

Le type et le thème du jeu sont libres (en sachant qu'il sera sur votre portfolio, donc pas n'importe quoi non plus).



##  Organisation

- Choisir le thème et le type du jeu

- Faire 2-3 maquettes fonctionnelles de votre jeu

- Créer un board dans le dépôt gitlab du jeu dans lequel vous listerez toutes les fonctionnalités à coder

- Créer une milestone (un Sprint) par semaine et y indiquer l'état dans lequel vous souhaitez que le jeu soit au bout de ce sprint, assigner les fonctionnalités du board à la milestone

  

##  Aspects techniques obligatoires

- Utilisation des objets Javascript d'une manière ou d'une autre

- Avoir des données dans le JS qui seront modifiées en cours de jeu (barre de vie, score, progression, etc.)

  

## Introduction

Snake est un jeu d'arcade dans lequel le joueur dirige une ligne qui grandit et constitue ainsi elle-même un obstacle. il est le premier mini-jeu que j'ai réalisé en utilisant la balise `<canvas></canvas>` du HTML 5 et Il est codé en JavaScript.

Cette méthode de réalisation (avec le canvas) permet au jeu de fonctionner en ligne sans avoir à utiliser des technologies comme Adobe Flash Player ou Java. Seul JavaScript doit être activé sur le navigateur Internet.

La chose importante à savoir avant de commencer est que le serpent est formé d’une chaîne d’éléments (carrés) et que le mouvement est autorisé en déplaçant le dernier carré du corps du serpent vers l’avant. Ce projet est également construit en utilisant les modèles de module pour la structure de code.



## Milestone

1. Creer des maquettes fonctionelles et UML.

2. Créez l'élément canvas dans html.

3. Créer la structure du serpent et de la nourriture.

4. Créer une fonction *collision* prennant en compte si le serpent s'est touché.

5. Créer la fonction principale qui doit exécuter tout ce dont j'ai besoin pour jouer.

6. Utiliser l' événement *keyCode* pour déplacer le serpent à l'aide du clavier.

7. créer des boutons pour la difficulté ( le snake se déplacera à une vitesse différente)

8. Incorporer des sons aux events.

   



## Maquettes fonctionnelles



### Choix de difficultés:



![difficulty](img/difficulty.png)







### En cours de Jeux:



![snake](img/snake.png)



### Fin de partie:

![gameOver](img/gameOver.jpg)



## UML

### Use Case:

![useCase](img/useCase.jpg)



### Object Diagram:

![diagrammeObject](img/diagrammeObject.jpg)



## Snake

### Comment dessiner le serpent avec la toile:

```
ctx.fillStyle = (i == 0) ? "black" : "green";
ctx.fillRect(snake[i].x, snake[i].y, box, box);

ctx.strokeStyle = "black";
ctx.strokeRect(snake[i].x, snake[i].y, box, box);
```

### La structure du serpent:

	let snake = [];
	
	snake[0] = {
	    x: 9 * box,
	    y: 10 * box
	};
### Le mouvement du serpent:

```
let d;

document.addEventListener("keydown", direction);

function direction(event) {
    let key = event.keyCode;
    if (key == 37 && d != "RIGHT") {
        left.play();
        d = "LEFT";
    } else if (key == 38 && d != "DOWN") {
        d = "UP";
        up.play();
    } else if (key == 39 && d != "LEFT") {
        d = "RIGHT";
        right.play();
    } else if (key == 40 && d != "UP") {
        d = "DOWN";
        down.play();
    }
}
```



## Exemples de données modifiées en cours de jeu:

### Score:

```
// Creation de la variable score
let score = 0;

// Affichage du score
ctx.fillStyle = "yellow";
ctx.font = "45px Changa one";
ctx.fillText(score, 2 * box, 1.6 * box);
```

### Si le serpent mange la souris:

```
 if (snakeX == food.x && snakeY == food.y) {
            score++;
            eat.play();
            food = {
                    x: Math.floor(Math.random() * 17 + 1) * box,
                    y: Math.floor(Math.random() * 15 + 3) * box
                }
```



## Exemples d'objets:

### Snake:

```
// create the snake

let snake = [];

snake[0] = {
    x: 9 * box,
    y: 10 * box
};
```

### Food:

```
let food = {
    x: Math.floor(Math.random() * 17 + 1) * box,
    y: Math.floor(Math.random() * 15 + 3) * box
}
```